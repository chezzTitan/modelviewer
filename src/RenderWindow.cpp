#include "RenderWindow.h"

RenderWindow::RenderWindow(int width, int height) : QMainWindow() {
    Model model;
    widget = new RenderWidget(this, model);
    setCentralWidget(widget);
    resize(width, height);

    bar = menuBar();
    menu = bar->addMenu("File");
    open = menu->addAction("Open 3D .Obj");
    connect(open, SIGNAL(triggered(bool)), this, SLOT(selectModel()));
    exit = menu->addAction("Exit");
    connect(exit, SIGNAL(triggered(bool)), this, SLOT(close()));
}

RenderWindow::~RenderWindow() {}

void RenderWindow::selectModel() {
    QString path = QFileDialog::getOpenFileName(this,"Select 3D Obj File",".",tr("Obj Files (*.obj)"));
    delete widget;
    Model model(path.toStdString());
    widget = new RenderWidget(this, model);
    setCentralWidget(widget);
}
